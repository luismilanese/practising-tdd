<?php
/*
Escreva um programa em sua linguagem favorita, que escreva números de 1 à 100.
Mas, para números múltiplos de 5, escreva "Ka" ao invés do número e para múltiplos de 7 escreva "Nois".
Para números que forem múltiplos de 5 e 7, escreva "KaNois".

NOTA Você deve escrever um número por linha, por exemplo:

x
y
z
[...]

 */

ini_set('display_errors', 0);
require_once 'Kanois.php';

if (count($argv) == 2 || count($argv) > 3) {
    die("Usage: index.php numberStart numberEnd" . PHP_EOL);
}

$start = isset($argv[1]) ? $argv[1] : 1;
$end = isset($argv[2]) ? $argv[2] : 100;

try {
    $kanois = new Kanois();
    $output = $kanois->process($start, $end);

    foreach ($output as $variable) {
        echo $variable . PHP_EOL;
    }
} catch (InvalidArgumentException $e) {
    die($e->getMessage() . PHP_EOL);
}
