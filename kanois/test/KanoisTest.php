<?php

require_once '../Kanois.php';

class KanoisTest extends PHPUnit_Framework_TestCase
{
    private $kanois;

    public function setUp()
    {
        $this->kanois = new Kanois();
    }

    public function testShouldOutputNumbersReplacingDividableFor3And5ForKaAndNois()
    {
        $output = $this->kanois->process(1, 100);

        $this->assertEquals(100, count($output));
        $this->assertEquals('Ka', $output[4]);
        $this->assertEquals('Nois', $output[6]);
        $this->assertEquals('KaNois', $output[34]);
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testShouldNotAcceptNonNumbericForArguments()
    {
        $this->kanois->process('one', 'a hundred');
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testEndNumberCannotBeLargerThanStartNumber()
    {
        $this->kanois->process(10, 1);
    }
}
