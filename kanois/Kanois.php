<?php

class Kanois
{
    public function process($start = 1, $end = 100)
    {
        $this->validateInput($start, $end);

        $output = [];
        for ($i = $start; $i <= $end; $i++) {
            $output[] = $this->evaluate($i);
        }

        return $output;
    }

    private function validateInput($start, $end)
    {
        if (!is_numeric($start) || !is_numeric($end)) {
            throw new InvalidArgumentException("Argument(s) should be numeric");
        }

        if ($start > $end) {
            throw new InvalidArgumentException("The start number cannot be greater than the end number");
        }

        return true;
    }

    private function evaluate($number)
    {
        if ($number % 5 === 0 && $number % 7 === 0) {
            return 'KaNois';
        }

        if ($number % 5 === 0) {
            return 'Ka';
        }

        if ($number % 7 === 0) {
            return 'Nois';
        }

        return $number;
    }
}
